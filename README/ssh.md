# SSH/SFTP Operations

The `Ssh` class purpose is to provide a *SSH/SFTP* client able to execute commands and transfer files using the *SSH* protocol.

Arguments:

  `username`: String. The user name to use for authentication on remote host.
  `password`: String. Optional, the password to use fo authentication on remote host. If the `key` argument is provided, then the `password` value will be use as the SSH key passphrase.
  `host`: String. The remote host to connect to.
  `port`: Int. Optional. The TCP remote port to connect to. Default: `22`.
  `key`: String. Optional. The SSH private key file name to use for authentication on remote host.
  `missing_host_key_policy`: String. Optional. Define the behavior when a remote key is missing and remote host is unknown. This should be one of `auto`, `warn`, `reject`. Default to `warn` which automatically add remote host key to the known keys and log a warning.

  Note:
    - When the `password` argument is provided alongside the `key` argument then it will be assumed that the provided password is a passphrase for the specified key.
    - The `missing_host_key_policy` allow one of the 4 following values:
      - `auto`: Automatically add remote host key to the known keys.
      - `warn`: Same as auto and log a warning.
      - `reject`: Reject the host key, do not proceed to connection and raise an error.


## Execute a command on a remote host

```python
from scrippy_remote.ssh import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
key = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key,
         password=password) as host:
  try:
    # When exit_on_error is set to False, the stderr output is returned as stdout['stderr']
    # and a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    stdout = host.exec_command(command="find /home/luigi.vercotti -type f",
                               exit_on_error=True)
    if stdout["exit_code"] == 0:
      for line in stdout["stdout"]:
        logging.debug(line)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Retrieve a remote file

```python
from scrippy_remote.ssh import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
remote_file = "/home/luigi.vercotti/piranha_brothers.txt"
key = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
local_dir = "/home/harry.fink"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key,
         password=password) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    #
    # When create_dirs is set to True, the exact dir hierachy of
    # the local file is recreated inside remote_dir before copying
    # the file into it
    host.get_file(remote_file=remote_file,
                  local_dir=local_dir,
                  create_dirs=False,
                  exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Mirror a remote directory to a local directory

```python
from scrippy_remote.ssh import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
key = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
remote_dir = "/home/luigi.vercotti"
local_dir = "/home/harry.fink"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key,
         password=password) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    host.local_mirror(remote_dir=remote_dir,
                      local_dir=local_dir,
                      exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Transfer a file to a remote host

```python
from scrippy_remote.remote import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
remote_dir = "/home/luigi.vercotti"
key = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
local_file = "/home/harry.fink/parrot.txt"
exit_on_error = True

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key,
         password=password) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    #
    # When create_dirs is set to True, the exact dir hierachy of
    # the local file is recreated inside remote_dir before copying
    # the file into it
    host.put_file(local_file=local_file,
                  remote_dir=remote_dir,
                  create_dirs=True,
                  exit_on_error=exit_on_error)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Mirror a local directory to a remote directory

```python
from scrippy_remote.ssh import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
key = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
remote_dir = "/home/luigi.vercotti"
local_dir = "/home/harry.fink"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key,
         password=password) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    host.remote_mirror(local_dir=local_dir,
                       remote_dir=remote_dir,
                       exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## List remote directory content

```python
from scrippy_remote.ssh import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
key = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
remote_dir = "/home/luigi.vercotti"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key,
         password=password) as host:
  # list all files matching `pattern` in a specific directory
  # Pattern is a regular expression *NOT* a glob or a shell pattern
  content = host.list_remote_dir(remote_dir=remote_dir,
                                 file_type="f",
                                 pattern=".*\.txt")
  # list all directory in a specific directory
  # Pattern is a regular expression *NOT* a glob or a shell pattern
  content = host.list_remote_dir(remote_dir=remote_dir,
                                 file_type="d",
                                 pattern=".*")
```


## Delete a remote file

```python
from scrippy_remote.ssh import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
key = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
remote_file = "/home/luigi.vercotti/parrot.text"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key,
         password=password) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    host.delete_remote_file(remote_file=remote_file,
                            exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Delete a remote directory recursively

```python
from scrippy_remote.ssh import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
key = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
remote_dir = "/home/luigi.vercotti/parrot.txt"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key,
         password=password) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    host.delete_remote_dir(remote_dir=remote_dir,
                           recursive=True,
                           exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```
