# CIFS Operations

The `Cifs` class purpose is to provide a *CIFS/Samba* client able to transfer files using the *CIFS/Samba* protocol.

  Arguments:
    `username`: String. Optional, default to 'anonymous'.
    `password`: String. Optional, default to 'anonymous'.
    `host`: String. The remote host to connect to.
    `port`: Int. Optional. The TCP remote port to connect to. Default to `445`.
    `shared_folder`: String. The remote shared folder to use.
    `use_ntlm_v2`: Boolean. Optional, default to True.
    `is_direct_tcp`: Boolean. Optional, Default to True.

*CIFS* support is functional but not guaranteed over time.

## Retrieve a remote file

```python
from scrippy_remote.cifs import Cifs
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 445
remote_user = "luigi.vercotti"
password = "dead_parrot"
shared_folder = "storage"
remote_file = "luiggi.vercotti/parrot.txt"
local_dir = "/home/harry.fink"

with Cifs(username=remote_user,
          host=remote_host,
          port=remote_port,
          shared_folder=shared_folder,
          password=password) as cifs:
  try:
    local_filepath = os.path.join(local_dir, "parrot.txt")
    cifs.create_local_dirs(remote_file=remote_file,
                           local_dir=local_dir)
    cifs.get_file(remote_filepath=remote_file,
                  local_filepath=local_dir)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Transfer a file to a remote host

```python
import os
from scrippy_remote.cifs import Cifs
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 445
remote_user = "luigi.vercotti"
password = "dead_parrot"
shared_folder = "storage"
remote_file = "luiggi.vercotti/parrot.txt"
local_file = "/home/harry.fink/parrot.txt"

with Cifs(username=remote_user,
          host=remote_host,
          port=remote_port,
          shared_folder=shared_folder,
          password=password) as cifs:
  try:
    cifs.create_remote_dir(os.path.dirname(remote_file))
    cifs.put_file(local_filepath=local_file,
                  remote_filepath=remote_file)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Open a file in write mode

```python
import os
from scrippy_remote.cifs import Cifs
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 445
remote_user = "luigi.vercotti"
password = "dead_parrot"
shared_folder = "storage"
remote_file = "luiggi.vercotti/parrot.txt"

with Cifs(username=remote_user,
          host=remote_host,
          port=remote_port,
          shared_folder=shared_folder,
          password=password) as cifs:
  try:
    with cifs.open(remote_file, mode="w") as w_file:
      w_file.write(b'None expect the Spannish inquisition')
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Open a file in read mode

```python
import os
from scrippy_remote.cifs import Cifs
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 445
remote_user = "luigi.vercotti"
password = "dead_parrot"
shared_folder = "storage"
remote_file = "luiggi.vercotti/parrot.txt"

with Cifs(username=remote_user,
          host=remote_host,
          port=remote_port,
          shared_folder=shared_folder,
          password=password) as cifs:
  try:
    with cifs.open(remote_file, mode="r") as r_file:
      content = r_file.readlines()
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Delete remote directory content

```python
from scrippy_remote.cifs import Cifs
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 445
remote_user = "luigi.vercotti"
password = "dead_parrot"
shared_folder = "storage"
remote_dir = "luiggi.vercotti"

with Cifs(username=remote_user,
          host=remote_host,
          port=remote_port,
          shared_folder=shared_folder,
          password=password) as cifs:
  try:
    cifs.delete_remote_dir_content(remote_dir)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```
