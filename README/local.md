# Local operations

Each of the `Ssh`, `Ftp`, `Cifs` classes comes with some helper functions to operate on the local host.

## List a local directory content with a pattern filter

Exemple with the `Ssh` class:

```python
from scrippy_remote.ssh import Ssh

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
key_filename = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
local_dir = "/home/harry.fink"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key_filename,
         password=password) as host:
  # List all files in a local directory
  # Pattern is a regular expression *NOT* a glob or a shell pattern
  files = host.list_local_dir(local_dir=local_dir,
                              file_type="f",
                              pattern=".*")
  # List all directories in a local directory
  # Pattern is a regular expression *NOT* a glob or a shell pattern
  dirs = host.list_local_dir(local_dir=local_dir,
                              file_type="d",
                              pattern=".*")
```

## delete a local file

Exemple with the `Ssh` class:

```python
from scrippy_remote.ssh import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
key_filename = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
local_file = "/home/harry.fink/parrot.txt"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key_filename,
         password=password) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    host.delete_local_file(local_file=local_file,
                           exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Delete a local directory recursively

Exemple with the `Ssh` class:

```python
import logging
from scrippy_remote.ssh import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 22
remote_user = "luigi.vercotti"
key_filename = "/home/luigi.vercotti/.ssh/id_rsa"
password = "dead_parrot"
local_dir = "/home/harry.fink"

with Ssh(username=remote_user,
         host=remote_host,
         port=remote_port,
         key=key_filename,
         password=password) as host:
  try:
    host.delete_local_dir(local_dir=local_dir,
                          recursive=True,
                          exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```
