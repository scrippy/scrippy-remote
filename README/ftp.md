# FTP Operations

The `Ftp` class purpose is to provide a FTP client able to support all type of operations with or without implicit or explicit TLS support.

  Arguments:
    `username`: String. Optional, default to 'anonymous'.
    `password`: String. Optional, default to 'anonymous'.
    `host`: String. The remote host to connect to.
    `port`: Int. Optional. The TCP remote port to connect to. Default: `21`.
    `tls`: Boolean. Optional, default to `True`. If set to `False`, the FTP connection will not be encrypted.
    `explicit`: Boolean. Optional, When set to `True` (default), the FTP connection will use explicit TLS (FTPes). If set to `False`, the connection will use implicit TLS (FTPs).
    `ssl_verify`: Boolean. Optional. Default to `True`. If set to `False`, remote server SSL certificate will not be verified.
    `ssl_version`: String. Optional. A string specifying the TLS version to use (Usually one of `TLSv1_3` or `TLSv1_2`). Default to auto negotiation between client and server for highest possible security.


## Retrieve a remote file

```python
from scrippy_remote.ftp import Ftp
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 21
remote_user = "luigi.vercotti"
remote_file = "/home/luigi.vercotti/piranha_brothers.txt"
password = "dead_parrot"
local_dir = "/home/harry.fink"

with Ftp(username=remote_user,
         host=remote_host,
         port=remote_port,
         password=password,
         tls=True,
         explicit=True,
         ssl_verify=True) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    #
    # When create_dirs is set to True, the exact dir hierachy of
    # the local file is recreated inside remote_dir before copying
    # the file into it
    host.get_file(remote_file=remote_file,
                  local_dir=local_dir,
                  create_dirs=False,
                  exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Mirror a remote directory to a local directory

```python
from scrippy_remote.ftp import Ftp
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 21
remote_user = "luigi.vercotti"
password = "dead_parrot"
remote_dir = "/home/luigi.vercotti"
local_dir = "/home/harry.fink"

with Ftp(username=remote_user,
         host=remote_host,
         port=remote_port,
         password=password,
         tls=True,
         explicit=True,
         ssl_verify=True) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    host.local_mirror(remote_dir=remote_dir,
                      local_dir=local_dir,
                      exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Transfer a file to a remote host

```python
from scrippy_remote.remote import Ssh
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 21
remote_user = "luigi.vercotti"
remote_dir = "/home/luigi.vercotti"
password = "dead_parrot"
local_file = "/home/harry.fink/parrot.txt"
exit_on_error = True

with Ftp(username=remote_user,
         host=remote_host,
         port=remote_port,
         password=password,
         tls=True,
         explicit=True,
         ssl_verify=True) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    #
    # Wher create_dirs is set to True, the exact dir hierachy of
    # the local file is recreated inside remote_dir before copying
    # the file into it
    host.put_file(local_file=local_file,
                  remote_dir=remote_dir,
                  create_dirs=True,
                  exit_on_error=exit_on_error)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Mirror a local directory to a remote directory

```python
from scrippy_remote.ftp import Ftp
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 21
remote_user = "luigi.vercotti"
password = "dead_parrot"
remote_dir = "/home/luigi.vercotti"
local_dir = "/home/harry.fink"

with Ftp(username=remote_user,
         host=remote_host,
         port=remote_port,
         password=password,
         tls=True,
         explicit=True,
         ssl_verify=True) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    host.remote_mirror(local_dir=local_dir,
                       remote_dir=remote_dir,
                       exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## List remote directory content

```python
from scrippy_remote.ftp import Ftp
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 21
remote_user = "luigi.vercotti"
password = "dead_parrot"
remote_dir = "/home/luigi.vercotti"

with Ftp(username=remote_user,
         host=remote_host,
         port=remote_port,
         password=password,
         tls=True,
         explicit=True,
         ssl_verify=True) as host:
  # list all files matching `pattern` in a specific directory
  # Pattern is a regular expression *NOT* a glob or a shell pattern
  content = host.list_remote_dir(remote_dir=remote_dir,
                                 file_type="f",
                                 pattern=".*\.txt")
  # list all directory in a specific directory
  # Pattern is a regular expression *NOT* a glob or a shell pattern
  content = host.list_remote_dir(remote_dir=remote_dir,
                                 file_type="d",
                                 pattern=".*")
```


## Delete a remote file

```python
from scrippy_remote.ftp import Ftp
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 21
remote_user = "luigi.vercotti"
password = "dead_parrot"
remote_file = "/home/luigi.vercotti/parrot.text"

with Ftp(username=remote_user,
         host=remote_host,
         port=remote_port,
         password=password,
         tls=True,
         explicit=True,
         ssl_verify=True) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    host.delete_remote_file(remote_file=remote_file,
                            exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```

## Delete a remote directory recursively

```python
from scrippy_remote.ftp import Ftp
from scrippy_remote import ScrippyRemoteError, logger

remote_host = "srv.flying.circus"
remote_port = 21
remote_user = "luigi.vercotti"
password = "dead_parrot"
remote_dir = "/home/luigi.vercotti/parrot.txt"

with Ftp(username=remote_user,
         host=remote_host,
         port=remote_port,
         password=password,
         tls=True,
         explicit=True,
         ssl_verify=True) as host:
  try:
    # When exit_on_error is set to False a warning is logged.
    # When set to True a ScrippyRemoteError exception is raised.
    host.delete_remote_dir(remote_dir=remote_dir,
                           recursive=True,
                           exit_on_error=True)
  except ScrippyRemoteError as err:
    logger.critical("{}".format(err))
```
