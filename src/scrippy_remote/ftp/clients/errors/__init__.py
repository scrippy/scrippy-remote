class Error(Exception):
  pass


class ErrorReply(Error):
  pass


class ErrorTemp(Error):
  pass


class ErrorPerm(Error):
  pass


class ErrorProto(Error):
  pass
