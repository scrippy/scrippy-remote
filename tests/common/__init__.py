import os
import shutil
import hashlib


working_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
local_file = os.path.join(working_dir, "parrot.txt")

env = {"working_dir": working_dir,
       "local_file": local_file,
       "md5_local_file": hashlib.md5(open(local_file, "rb").read()).hexdigest(),
       "remote_dir": "dead/parrot",
       "password": "d34dp4rr0t",
       "local_dir": "/tmp/tests",
       "pattern": r".*\.txt"}

shutil.rmtree(env.get("local_dir"), ignore_errors=True)
