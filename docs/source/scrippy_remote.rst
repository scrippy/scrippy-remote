scrippy\_remote package
=======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   scrippy_remote.cifs
   scrippy_remote.ftp
   scrippy_remote.local
   scrippy_remote.ssh

Module contents
---------------

.. automodule:: scrippy_remote
   :members:
   :undoc-members:
   :show-inheritance:
